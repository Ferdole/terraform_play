terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-west-2"
}

resource "aws_s3_bucket" "b" {
  bucket = "ferdole-test-bucket"
  acl    = "private"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}


resource "aws_s3_bucket_object" "lambda_zip" {
  bucket = "${aws_s3_bucket.b.id}"
  key    = "lambda-function/lambda_function.zip"
  source = "out.zip"
  source_hash = "${module.ferdo1.source_code_hash}"
}


resource "aws_s3_bucket_object" "glue_script" {
  bucket = "${aws_s3_bucket.b.id}"
  key    = "glueScript/glueTest.py"
  source = "ferdole_l1/py.py"
  # source_hash = filebase64sha256("ferdole_l1/py.py")
  etag = filemd5("ferdole_l1/py.py")
}

resource "aws_s3_bucket_object" "empty_folder" {
  for_each = {
    k1 = "in",
    k2 = "out",
  }

  bucket       = "${aws_s3_bucket.b.id}"
  key          = "test/${each.key}/${each.value}/"
  acl          = "private"
  content_type = "application/x-directory" 
}


# data "archive_file" "lambda_code" {
#   type        = "zip"
#   source_dir = "ferdole_l1"
#   output_path = "output.zip"

#   depends_on = [
#     null_resource.l1
#   ]

# }

##### python packing #####
module "ferdo1" {
  source = "rojopolis/lambda-python-archive/aws"

  src_dir              = "${path.module}/ferdole_l1"
  output_path          = "${path.module}/out.zip"
  install_dependencies = true
}

# provisioner lambda
resource "aws_lambda_function" "ferdole_l1" {
  # filename         = "output.zip"
  description      = "test lambda"
  s3_bucket        = "ferdole-test-bucket"
  s3_key           = "${aws_s3_bucket_object.lambda_zip.key}"
  function_name    = "ferdole_l1"
  role             = "arn:aws:iam::035236242821:role/service-role/ferdole_test1-role-prkoz8yp"
  handler          = "lambda_function.lambda_handler"
  runtime          = "python3.8"
  
  # source_code_hash = "${data.archive_file.lambda_code.output_base64sha256}"
  # source_code_hash = filebase64sha256("out.zip")
  # source_code_hash = "${resource.null_resource.l1.output_base64sha256}"
  source_code_hash = "${module.ferdo1.source_code_hash}"

}

resource "aws_glue_job" "ferdole_glue" {
  name     = "otherName"
  role_arn = "arn:aws:iam::035236242821:role/service-role/ferdole_test1-role-prkoz8yp"

  command {
    script_location = "s3://${aws_s3_bucket.b.bucket}/${aws_s3_bucket_object.glue_script.key}"
  }
}

# resource "null_resource" "l1" {

#   triggers = {
#     always_run = "${timestamp()}"
#   }

#   provisioner "local-exec" {
#     command = "echo \"print('dorel')\" > ferdole_l1/pyt.py"
#   }
# }


# data "archive_file" "lambda_code" {
#     type        = "zip"
#     source_file = "ferdole_l1/lambda_function.py"
#     output_path = "output.zip"

# }

# resource "aws_lambda_function" "ferdole_l1" {
#     # filename         = "output.zip"
#     description      = "test lambda"
#     s3_bucket        = "ferdole-test-bucket"
#     s3_key           = "${aws_s3_bucket_object.lambda_zip.key}"
#     function_name    = "ferdole_l1"
#     role             = "arn:aws:iam::035236242821:role/service-role/ferdole_test1-role-prkoz8yp"
#     handler          = "lambda_function.lambda_handler"
#     runtime          = "python3.8"
    
#     source_code_hash = "${data.archive_file.lambda_code.output_base64sha256}"
#     # source_code_hash = filebase64sha256("output.zip")
    
#     depends_on       = [
#       data.archive_file.lambda_code
#     ]
# }

# resource "aws_instance" "app_server" {
#   ami           = "ami-830c94e3"
#   instance_type = "t2.micro"

#   tags = {
#     Name = "ExampleAppServerInstance"
#   }
# }